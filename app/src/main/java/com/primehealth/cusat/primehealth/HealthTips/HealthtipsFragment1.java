package com.primehealth.cusat.primehealth.HealthTips;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.primehealth.cusat.primehealth.ApiClient;
import com.primehealth.cusat.primehealth.ApiInterface;
import com.primehealth.cusat.primehealth.FindMedical.MedicalClickListener;
import com.primehealth.cusat.primehealth.R;
import com.primehealth.cusat.primehealth.RealmUtils;

import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 22-May-18.
 */
public class HealthtipsFragment1 extends Fragment {

    private RecyclerView.LayoutManager layoutManager;
    private health_tips_adapter adapter;
    private List<HealthTipsRealmobj> healthTipsRealmobjs;
    private ApiInterface apiInterface;
    private RealmResults<HealthTipsRealmobj> healthTipsRealmobjRealmResults;
    private HealthTipsRealmobj healthTipsRealmobj;
    private RecyclerView recyclerView;


    public HealthtipsFragment1(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.activity_healthtips_fragment1,container,false);
        initViews(view);
        return  view;
    }

    private void initViews(View view) {
        recyclerView=(RecyclerView)view.findViewById(R.id.rvhealthtips);
        layoutManager=new LinearLayoutManager(getContext());
        // final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        Call<ModelHealthTip> call=apiInterface.getHealthTip();
        call.enqueue(new Callback<ModelHealthTip>() {
            @Override
            public void onResponse(Call<ModelHealthTip> call, Response<ModelHealthTip> response) {
                RealmUtils.getInstance().saveHealthTips(response.body().getHealth_tip());
            }

            @Override
            public void onFailure(Call<ModelHealthTip> call, Throwable t) {

            }
        });

        healthTipsRealmobjRealmResults=RealmUtils.getInstance().getAllHealthTips();
        adapter=new health_tips_adapter(healthTipsRealmobjRealmResults,
                new MedicalClickListener()
                {
                    @Override
                    public void onClick(String id) {
                        healthTipsRealmobj=  RealmUtils.getInstance().getItemWithIdHealthTip(id);
                        Toast.makeText(getContext(), healthTipsRealmobj.getHealth_tips_heading(), Toast.LENGTH_SHORT).show();
                        String health_tips_heading=healthTipsRealmobj.getHealth_tips_heading();
                        String health_tips_small_desc=healthTipsRealmobj.getHealth_tips_small_desc();
                        String health_tips_desc=healthTipsRealmobj.getHealth_tips_desc();


                        Intent intent = new Intent(getActivity(),HealthTipsP2.class);
                        intent.putExtra("tvhealtipp2heading",health_tips_heading);
                        intent.putExtra("tvhealtipp2dscr",health_tips_desc);
                        startActivity(intent);

                    }
                }
        );
recyclerView.setAdapter(adapter);

    }

}
