package com.primehealth.cusat.primehealth.FindDoctor;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Doctor_realm extends RealmObject
{
    @PrimaryKey
    private String doctor_id;
    private String doctor_name;
    private String department_id;
    private String medicalcenter_id;
    private String degree;
    private String telephone_no;
    private String email;
    private String Description;
    private String status;
    private String department_name;
    private String medicalcenter_name;

    public String getDoctor_id() {
        return doctor_id;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public String getMedicalcenter_id() {
        return medicalcenter_id;
    }

    public String getDegree() {
        return degree;
    }

    public String getTelephone_no() {
        return telephone_no;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return Description;
    }

    public String getStatus() {
        return status;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public String getMedicalcenter_name() {
        return medicalcenter_name;
    }
}
