package com.primehealth.cusat.primehealth;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abhij on 28/05/2018.
 */

public class ApiClient {
    public static final String BASE_URL="http://172.16.1.111/primehealth/";
    //public static final String BASE_URL="https://primehealth.000webhostapp.com/primehealth/";
    public static Retrofit retrofit=null;

    public static Retrofit getApiClient()
    {
        if (retrofit==null)
        {
            retrofit=new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
