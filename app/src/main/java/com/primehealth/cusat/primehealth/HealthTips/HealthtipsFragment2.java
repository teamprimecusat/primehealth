package com.primehealth.cusat.primehealth.HealthTips;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.primehealth.cusat.primehealth.R;

/**
 * Created by User on 22-May-18.
 */

public class HealthtipsFragment2 extends Fragment {

    public HealthtipsFragment2(){

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        return inflater.inflate(R.layout.activity_healthtips_fragment2, container, false);
    }
}
