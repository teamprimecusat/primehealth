package com.primehealth.cusat.primehealth;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Clinic_Info extends AppCompatActivity {

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Clinic Info");

        setContentView(R.layout.activity_clinic__info);
        Intent intent=getIntent();
        bundle=intent.getExtras();

        String email = bundle.getString("email");
        String fax  = bundle.getString("fax");
        String frieday=bundle.getString("frieday");
        String location=bundle.getString("location");
        String medicalcentrename=bundle.getString("medicalcentrename");
        String saturday = bundle.getString("saturday");
        String teleno = bundle.getString("teleno");
        String website=bundle.getString("website");
        String weekdays=bundle.getString("weekdays");
//        Toast.makeText(this,weekdays, Toast.LENGTH_SHORT).show();


        TextView tvtvheading_hospitalname=(TextView)findViewById(R.id.tvheading_hospitalname);
        TextView tvlocation=(TextView)findViewById(R.id.tvlocation);
        TextView tvtelno=(TextView)findViewById(R.id.tvtelno);
        TextView tvfaxno=(TextView)findViewById(R.id.tvfaxno);
        TextView tvemial=(TextView)findViewById(R.id.tvemial);
        TextView tvwebsite=(TextView)findViewById(R.id.tvwebsite);

        TextView tvweekdays=(TextView)findViewById(R.id.tvweekdays);
        TextView tvfrieday=(TextView)findViewById(R.id.tvfrieday);
        TextView tvsaturday=(TextView)findViewById(R.id.tvsaturday);

        tvtvheading_hospitalname.setText(medicalcentrename);
        tvlocation.setText(location);
        tvtelno.setText(teleno);
        tvfaxno.setText(fax);
        tvemial.setText(email);
        tvwebsite.setText(website);
        tvweekdays.setText(weekdays);
        tvfrieday.setText(frieday);
        tvsaturday.setText(saturday);

    }
    public void imagelocation()
    {
        Toast.makeText(this, "Image button click", Toast.LENGTH_SHORT).show();
    }
}
