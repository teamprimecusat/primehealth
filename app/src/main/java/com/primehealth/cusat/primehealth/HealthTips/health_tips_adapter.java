package com.primehealth.cusat.primehealth.HealthTips;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.primehealth.cusat.primehealth.FindMedical.MedicalClickListener;
import com.primehealth.cusat.primehealth.R;

import java.util.List;

public class health_tips_adapter extends RecyclerView.Adapter<health_tips_adapter.MyViewHolder_healthtip_asptr>{
private List<HealthTipsRealmobj> healthTipsRealmobjList;
private MedicalClickListener medicalClickListener;

    public health_tips_adapter(List<HealthTipsRealmobj> healthTipsRealmobjList,MedicalClickListener medicalClickListener) {
        this.healthTipsRealmobjList = healthTipsRealmobjList;
        this.medicalClickListener=medicalClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder_healthtip_asptr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_tips_layout_list_item,parent,false);
        return new MyViewHolder_healthtip_asptr(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder_healthtip_asptr holder, final int position) {
    holder.tipheading.setText(healthTipsRealmobjList.get(position).getHealth_tips_heading());
    holder.tipasdescription.setText(healthTipsRealmobjList.get(position).getHealth_tips_small_desc());
    holder.llhealthtip.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            medicalClickListener.onClick(healthTipsRealmobjList.get(position).getHealth_tips_id());
        }
    });
    }

    @Override
    public int getItemCount() {
        return healthTipsRealmobjList.size();
    }

    public class MyViewHolder_healthtip_asptr extends RecyclerView.ViewHolder
{
    TextView tipheading,tipasdescription;
    private LinearLayout llhealthtip;
    public MyViewHolder_healthtip_asptr(View itemView) {
        super(itemView);
        tipheading=(TextView)itemView.findViewById(R.id.tvtipsheading);
        tipasdescription=(TextView)itemView.findViewById(R.id.tvtipasdescription);
        llhealthtip=(LinearLayout)itemView.findViewById(R.id.llhealthtip);

    }
}

}
