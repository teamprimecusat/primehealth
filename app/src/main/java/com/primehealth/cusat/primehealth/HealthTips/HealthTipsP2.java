package com.primehealth.cusat.primehealth.HealthTips;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.primehealth.cusat.primehealth.R;

public class HealthTipsP2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_tips_p2);

        Intent intent = getIntent();
        Bundle bundle=intent.getExtras();
        String heading = bundle.getString("tvhealtipp2heading");
        String desc=bundle.getString("tvhealtipp2dscr");
        TextView tvhealtipp2heading=(TextView)findViewById(R.id.tvhealtipp2heading);
        TextView tvhealtipp2dscr = (TextView)findViewById(R.id.tvhealtipp2dscr);
        tvhealtipp2heading.setText(heading);
        tvhealtipp2dscr.setText(desc);


    }
}
