package com.primehealth.cusat.primehealth.FindMedical;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.primehealth.cusat.primehealth.Clinic_Info;
import com.primehealth.cusat.primehealth.ApiClient;
import com.primehealth.cusat.primehealth.ApiInterface;
import com.primehealth.cusat.primehealth.R;
import com.primehealth.cusat.primehealth.RealmUtils;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindMedicalCenter extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap map;
    private ArrayList<String> mcNames=new ArrayList<>();
    private ArrayList<String> mcAddres= new ArrayList<>();
    private ArrayList<String> mcImageurl = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private medical_adapter adapter;
    private List<MedicalCentre> medicalCentres;
    private ApiInterface apiInterface;
    private RealmResults<MedicalCentre>medicalCentreRealmResults;
    private MedicalCentre medicalCentre;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_medical_center);

        getSupportActionBar().setTitle("Find Medical Center");

        SupportMapFragment mapFragment =(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.g_map);
        mapFragment.getMapAsync(this);
        initImageBitmaps();

        recyclerView=(RecyclerView)findViewById(R.id.recycler_view_medical_center);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);

        Call<Modelmedical> call = apiInterface.getMedicals();
        call.enqueue(new Callback<Modelmedical>() {
            @Override
            public void onResponse(Call<Modelmedical> call, Response<Modelmedical> response) {

                RealmUtils.getInstance().saveModelMeicals(response.body().getMedical_center());
            }

            @Override
            public void onFailure(Call<Modelmedical> call, Throwable t) {

            }
        });



    }

    private void initImageBitmaps()
    {
//        mcImageurl.add("https://i.redd.it/qn7f9oqu7o501.jpg");
//        mcNames.add("Abu dhabi");
//        mcAddres.add("Abu dhabi al Falah street");
//
//        mcImageurl.add("https://i.redd.it/qn7f9oqu7o501.jpg");
//        mcNames.add("Abu dhabi");
//        mcAddres.add("Abu dhabi al Falah street");
//
//        mcImageurl.add("https://i.redd.it/qn7f9oqu7o501.jpg");
//        mcNames.add("Abu dhabi");
//        mcAddres.add("Abu dhabi al Falah street");
        initRecyclerview();
    }
    private void initRecyclerview()
    {

        RecyclerView recyclerView = findViewById(R.id.recycler_view_medical_center);
        medicalCentreRealmResults = RealmUtils.getInstance().getAllMedicals();
        adapter = new medical_adapter(medicalCentreRealmResults, new MedicalClickListener() {
            @Override
            public void onClick(String id) {
                medicalCentre=RealmUtils.getInstance().getItemWithId(id);
             //   Toast.makeText(FindMedicalCenter.this, medicalCentre.getLocation(), Toast.LENGTH_SHORT).show();
Intent intent = new Intent(FindMedicalCenter.this,Clinic_Info.class);
intent.putExtra("email",medicalCentre.getEmail());
intent.putExtra("fax",medicalCentre.getFax_no());
intent.putExtra("frieday",medicalCentre.getFriday());
intent.putExtra("location",medicalCentre.getLocation());
intent.putExtra("medicalcentrename",medicalCentre.getMedicalcenter_name());
intent.putExtra("saturday",medicalCentre.getSaturday());
intent.putExtra("teleno",medicalCentre.getTelephone_no());
intent.putExtra("website",medicalCentre.getWebsite());
intent.putExtra("weekdays",medicalCentre.getWeekdays());
startActivity(intent);
            }
        });

//        RecyclerviewAdapter_find_medical_center adapter= new RecyclerviewAdapter_find_medical_center(this,mcImageurl,mcNames,mcAddres);
        recyclerView.setAdapter(adapter);
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        map.setMinZoomPreference(13);

        LatLng seattle = new LatLng(10.023078, 76.336286);
        LatLng seattle1 = new LatLng(10.019361, 76.343487);
        map.addMarker(new MarkerOptions().position(seattle).title("SEATTLE"));
        map.addMarker(new MarkerOptions().position(seattle1).title("SEATTLE"));
        map.moveCamera(CameraUpdateFactory.newLatLng(seattle));
        map.moveCamera(CameraUpdateFactory.newLatLng(seattle1));
    }
    public void clinicinfo(View view)
    {
        Intent intent = new Intent(this,Clinic_Info.class);
        startActivity(intent);
    }
}
