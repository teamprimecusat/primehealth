package com.primehealth.cusat.primehealth;

import com.primehealth.cusat.primehealth.FindDoctor.Doctor_realm;
import com.primehealth.cusat.primehealth.FindMedical.MedicalCentre;
import com.primehealth.cusat.primehealth.HealthTips.HealthTipsRealmobj;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

;

/**
 * Created by sanif on 03/05/18.
 */

public class RealmUtils {

    private static final String TAG = "RealmUtils";
    private static RealmUtils instance = null;
    private Realm realm;

    private RealmUtils() {
        realm = Realm.getDefaultInstance();
    }


    public static synchronized RealmUtils getInstance() {
        return new RealmUtils(); //No need to create a singleton instance for realm util. Will cause threading issues.
    }

    public void saveModelMeicals(final ArrayList<MedicalCentre> modelMedicals) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(modelMedicals);
            }
        });
    }
    public void saveDoctors(final ArrayList<Doctor_realm> doctor_realms)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(doctor_realms);
            }
        });
    }

    public void saveHealthTips(final ArrayList<HealthTipsRealmobj> healthTipsRealmobjs)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(healthTipsRealmobjs);
            }
        });
    }


//    public void saveModelDoctors(final ArrayList<DoctorProfile> modelDoctors)
//    {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealmOrUpdate(modelDoctors);
//            }
//        });
//    }

//    public RealmResults<DoctorProfile> getAllDoctors()
//    {
//        return realm.where(DoctorProfile.class).findAll();
//    }

    public RealmResults<Doctor_realm> getfinddrwithmedicalcenter(String medical_center) {
        return realm.where(Doctor_realm.class).equalTo("medicalcenter_name",medical_center).findAll();
    }

    public RealmResults<MedicalCentre> getAllMedicals() {
        return realm.where(MedicalCentre.class).findAll();
    }
    public RealmResults<Doctor_realm> getAllDoctors()
    {
        return realm.where(Doctor_realm.class).findAll();
    }

    public RealmResults<HealthTipsRealmobj> getAllHealthTips() {
        return realm.where(HealthTipsRealmobj.class).findAll();
    }

    public MedicalCentre getItemWithId(String mItemId) {
        return realm.where(MedicalCentre.class).equalTo("medicalcenter_id",mItemId).findFirst();
    }

    public Doctor_realm getDr_ItemwithId(String DrItemId)
    {
        return realm.where(Doctor_realm.class).equalTo("doctor_id",DrItemId).findFirst();
    }
    public HealthTipsRealmobj getItemWithIdHealthTip(String hItemId)
    {
        return realm.where(HealthTipsRealmobj.class).equalTo("health_tips_id",hItemId).findFirst();
    }

//    public DoctorProfile getItemWithIdDR(String dItemId)
//    {
//        return realm.where(DoctorProfile.class).equalTo("doctor_id",dItemId).findFirst();
//    }

}

//    //Mmenu Items
//    public void addUserMenu(final SmartHealthMenu healthMenu) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealmOrUpdate(healthMenu);
//            }
//        });
//    }
//
//    public void updateMenuStatus(final int pkey, final boolean isEnabled) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                SmartHealthMenu healthMenu = realm.where(SmartHealthMenu.class).equalTo("menuId", pkey).findFirst();
//                healthMenu.setEnabled(isEnabled);
//            }
//        });
//
//    }
//
//    public void updateGoal(final int pKey, final float goal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                SmartHealthMenu healthMenu = realm.where(SmartHealthMenu.class).equalTo("menuId", pKey).findFirst();
//                healthMenu.setGoal(goal);
//            }
//        });
//    }
//
//    public void updateMenuIcons(final int pKey, final int resourceID) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                SmartHealthMenu healthMenu = realm.where(SmartHealthMenu.class).equalTo("menuId", pKey).findFirst();
//                healthMenu.setImageId(resourceID);
//            }
//        });
//    }
//
//    public RealmResults<SmartHealthMenu> getAllMenus() {
//        return realm.where(SmartHealthMenu.class).findAll();
//    }
//
//    //========Update Menu if Action data available on other Tables==========
//    public void updateMenuCurrentValue(final int menuID, final float currentValue) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                SmartHealthMenu menu = realm.where(SmartHealthMenu.class).equalTo("menuId", menuID).findFirst();
//                menu.setCurrentValue(currentValue);
//            }
//        });
//    }
//
//    //=======================Walking SMamrt Health Data==============================
//    public WalkingDataSmartHealth getTodaysWalkingData(String date) {
//        return realm.where(WalkingDataSmartHealth.class).equalTo("date", date).findFirst();
//    }
//
//    public void updateWalkingGoal(final String date, final float goal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                WalkingDataSmartHealth health = realm.where(WalkingDataSmartHealth.class).equalTo("date", date).findFirst();
//                if (health != null) {
//                    health.setWalkGoal((int) goal);
//                }
//
//            }
//        });
//    }
//
//    public void insertWalkingData(final WalkingDataSmartHealth walkingDataSmartHealth) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealm(walkingDataSmartHealth);
//            }
//        });
//    }
//
//    public void updateWalkingData(final WalkingDataSmartHealth smartHealth, final String date) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                WalkingDataSmartHealth health = realm.where(WalkingDataSmartHealth.class).equalTo("date", date).findFirst();
//                health.setCalorieBurned(smartHealth.getCalorieBurned());
//                health.setWalkDistance(smartHealth.getWalkDistance());
//                health.setElapsedTime(smartHealth.getElapsedTime());
//                health.setAverageSpeed(smartHealth.getAverageSpeed());
//            }
//        });
//    }
//
//    //========================Runnig Health Data===========================================//
//    public RunningDataSmartHealth getTodaysRunningData(String date) {
//        return realm.where(RunningDataSmartHealth.class).equalTo("date", date).findFirst();
//    }
//
//    public void updateRunningGoal(final String date, final float goal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                RunningDataSmartHealth health = realm.where(RunningDataSmartHealth.class).equalTo("date", date).findFirst();
//                if (health != null) {
//                    health.setWalkGoal((int) goal);
//                }
//
//            }
//        });
//    }
//
//    public void insertNewRunningData(final RunningDataSmartHealth runningData) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealm(runningData);
//            }
//        });
//    }
//
//    public void updateRunningData(final RunningDataSmartHealth runningData, final String date) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                RunningDataSmartHealth data = realm.where(RunningDataSmartHealth.class).equalTo("date", date).findFirst();
//                data.setCalorieBurned(runningData.getCalorieBurned());
//                data.setWalkDistance(runningData.getWalkDistance());
//                data.setElapsedTime(runningData.getElapsedTime());
//                data.setAverageSpeed(runningData.getAverageSpeed());
//            }
//        });
//    }
//
////    =========================CYCLING DATA=======================================//
//
//    public CyclingDataSmartHealth getTodaysCyclingData(String date) {
//        return realm.where(CyclingDataSmartHealth.class).equalTo("date", date).findFirst();
//    }
//
//    public void insertNewCyclingData(final CyclingDataSmartHealth cyclingDataSmartHealth) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealm(cyclingDataSmartHealth);
//            }
//        });
//
//    }
//
//    public void updateCyclingGoal(final String date, final float goal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                CyclingDataSmartHealth health = realm.where(CyclingDataSmartHealth.class).equalTo("date", date).findFirst();
//                if (health != null) {
//                    health.setWalkGoal((int) goal);
//                }
//
//            }
//        });
//    }
//
//    public void updateCyclingData(final CyclingDataSmartHealth cyclingData, final String date) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                CyclingDataSmartHealth data = realm.where(CyclingDataSmartHealth.class).equalTo("date", date).findFirst();
//                data.setCalorieBurned(cyclingData.getCalorieBurned());
//                data.setWalkDistance(cyclingData.getWalkDistance());
//                data.setElapsedTime(cyclingData.getElapsedTime());
//                data.setAverageSpeed(cyclingData.getAverageSpeed());
//            }
//        });
//    }
//
//
////    =================PEDO-METER================
//
//    public void insertNewPedoData(final PedometerModal pedometerModal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealm(pedometerModal);
//            }
//        });
//    }
//
//    public void updatePedometerData(final String date, final PedometerModal pedometerModal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                PedometerModal modal = realm.where(PedometerModal.class).equalTo("date", date).findFirst();
//                modal.setSteps(pedometerModal.getSteps());
//                modal.setCalorieBurned(pedometerModal.getCalorieBurned());
//            }
//        });
//    }
//
//    public PedometerModal getPedoData(String date) {
//        return realm.where(PedometerModal.class).equalTo("date", date).findFirst();
//    }
//
//    public void updatePedoGoal(final int goal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                PedometerModal modal = realm.where(PedometerModal.class).equalTo("date", DateUtils.getCurrentDateRealmFormat()).findFirst();
//                modal.setStepGoal(goal);
//            }
//        });
//
//    }
//
//
////    ======= DAY STEP TRACKER=========//
//
//    public void inserStep(final DayStepTracker dayStep) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealm(dayStep);
//            }
//        });
//    }
//
//    public RealmResults<DayStepTracker> getAllTrackedStep() {
//
//        return realm.where(DayStepTracker.class).findAll();
//    }
//
//    public void deleteAllTrackedStep() {
//
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                RealmResults<DayStepTracker> dayStepTrackers = realm.where(DayStepTracker.class).findAll();
//                dayStepTrackers.deleteAllFromRealm();
//            }
//        });
//    }
//
//
//    public void saveClubRegistration(final ClubRegisterData clubRegisterData) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealmOrUpdate(clubRegisterData);
//            }
//        });
//    }
//
//    public ClubRegisterData getclubRegisterData() {
//        return realm.where(ClubRegisterData.class).findFirst();
//    }
//
//
//    public void clearClubRegistration() {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.where(ClubRegisterData.class).findAll().deleteAllFromRealm();
//            }
//        });
//    }
//
//
//    //proxy problem so saving as two object(to be fixed)
//    public void saveClubRegistrationAddress(final ClubRegisterAdress clubRegisterAdress) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealmOrUpdate(clubRegisterAdress);
//            }
//        });
//    }
//
//    public ClubRegisterAdress getclubRegisterDataAddress() {
//        return realm.where(ClubRegisterAdress.class).findFirst();
//    }
//
//
//    public void clearClubRegistrationAdress() {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.where(ClubRegisterAdress.class).findAll().deleteAllFromRealm();
//            }
//        });
//    }
//
//    //    PROFILE PAGE
//    public void updateProfileData(final ProfileModal profileModal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                ProfileModal pModal = realm.where(ProfileModal.class).equalTo("id", 1).findFirst();
//                pModal.setDateOfBirth(profileModal.getDateOfBirth());
//                pModal.setGender(profileModal.getGender());
//                pModal.setHeight(profileModal.getHeight());
//                pModal.setProfileImage(profileModal.getProfileImage());
//                pModal.setHeightUnit(profileModal.getHeightUnit());
//                pModal.setWeight(profileModal.getWeight());
//                pModal.setWeightUnit(profileModal.getWeightUnit());
//                pModal.setUserName(profileModal.getUserName());
//                pModal.setEmail(profileModal.getEmail());
//            }
//        });
//    }
//
//    public void insertProfileData(final ProfileModal profileModal) {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.copyToRealmOrUpdate(profileModal);
//            }
//        });
//    }
//
//    public ProfileModal getProfile() {
//        return realm.where(ProfileModal.class).equalTo("id", 1).findFirst();
//    }
//
//    //====================LOGIN===========================
//
//    public void saveUser(final LoginData loginData) {
//
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                realm.delete(LoginData.class);
//                realm.insertOrUpdate(loginData);
//            }
//        });
//    }
//
//    public LoginData getLoginData()
//    {
//        return realm.where(LoginData.class).findFirst();
//    }
//
//    public String getUserToken()
//    {
//        return realm.where(LoginData.class).findFirst().getAccessToken();
//    }




