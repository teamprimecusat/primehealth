package com.primehealth.cusat.primehealth.FindDoctor;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.primehealth.cusat.primehealth.FindMedical.MedicalClickListener;
import com.primehealth.cusat.primehealth.R;

import java.util.List;

public class doctor_adapter extends RecyclerView.Adapter<doctor_adapter.MyViewHolder_doctor_adptr>
{
private List<Doctor_realm> doctor_realmList;
private MedicalClickListener medicalClickListener;

    public doctor_adapter(List<Doctor_realm> doctor_realmList, MedicalClickListener medicalClickListener) {
        this.doctor_realmList = doctor_realmList;
        this.medicalClickListener = medicalClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder_doctor_adptr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_layout_list_item,parent,false);
        return new MyViewHolder_doctor_adptr(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder_doctor_adptr holder, final int position) {
        holder.dr_name.setText(doctor_realmList.get(position).getDoctor_name());
        holder.dr_degree.setText(doctor_realmList.get(position).getDegree());
        holder.dr_department.setText(doctor_realmList.get(position).getDepartment_name());
        holder.dr_hospital_name.setText(doctor_realmList.get(position).getMedicalcenter_name());
        holder.lldoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicalClickListener.onClick(doctor_realmList.get(position).getDoctor_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return doctor_realmList.size();
    }

    public class MyViewHolder_doctor_adptr extends RecyclerView.ViewHolder
    {

        TextView dr_name,dr_degree,dr_department,dr_hospital_name;
        private LinearLayout lldoctor;
        public MyViewHolder_doctor_adptr(View itemView) {
            super(itemView);
            dr_name=(TextView)itemView.findViewById(R.id.tv_dr_name);
            dr_degree=(TextView)itemView.findViewById(R.id.tv_dr_degree);
            dr_department=(TextView)itemView.findViewById(R.id.tv_dr_department);
            dr_hospital_name=(TextView)itemView.findViewById(R.id.tv_dr_hospital_name);
            lldoctor=(LinearLayout)itemView.findViewById(R.id.doctor_paraent_layout);

        }
    }

}
