package com.primehealth.cusat.primehealth;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by abhij on 22/05/2018.
 */

public class RecyclerviewAdapter_find_medical_center extends RecyclerView.Adapter<RecyclerviewAdapter_find_medical_center.medical_viewHolder> {
    private static final String TAG="RecyclerViewAdapter";
    private ArrayList<String> mImageNames=new ArrayList<>();//name of the medical center
    private ArrayList<String>mImages=new ArrayList<>();//url of the images
    private ArrayList<String>mAddress=new ArrayList<>();//medical center address
    private Context context;
    public RecyclerviewAdapter_find_medical_center(Context context,ArrayList<String> mImageNames,ArrayList<String>mImages,ArrayList<String>mAddress)
    {
        this.mImageNames=mImageNames;
        this.mImages=mImages;
        this.mAddress=mAddress;
        this.context=context;
    }

    @NonNull
    @Override
    public medical_viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.medical_center_layout_list_item,parent,false);
        medical_viewHolder holder = new medical_viewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull medical_viewHolder holder, final int position) {
        Glide.with(context).asBitmap().load(mImages.get(position)).into(holder.image);
        holder.imagename.setText(mImageNames.get(position));
        holder.mAddress.setText(mAddress.get(position));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, mImageNames.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    public class medical_viewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView image;
        TextView imagename;
        TextView mAddress;
        LinearLayout parentLayout;
        //RelativeLayout parentLayout;


        public medical_viewHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.medical_image);
            imagename=itemView.findViewById(R.id.medical_hospital_name);
            mAddress=itemView.findViewById(R.id.medical_hospital_address);
            parentLayout=itemView.findViewById(R.id.medical_center_parent_layout);
        }

    }


}
