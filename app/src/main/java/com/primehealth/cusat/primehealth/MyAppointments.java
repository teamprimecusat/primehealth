package com.primehealth.cusat.primehealth;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MyAppointments extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_appointments);

        getSupportActionBar().setTitle("My Appointments");
    }
}
