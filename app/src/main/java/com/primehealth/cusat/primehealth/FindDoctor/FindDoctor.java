package com.primehealth.cusat.primehealth.FindDoctor;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import com.primehealth.cusat.primehealth.ApiClient;
import com.primehealth.cusat.primehealth.ApiInterface;
import com.primehealth.cusat.primehealth.FindMedical.MedicalClickListener;
import com.primehealth.cusat.primehealth.R;
import com.primehealth.cusat.primehealth.RealmUtils;
import com.primehealth.cusat.primehealth.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindDoctor extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private RecyclerView.LayoutManager layoutManager;
    private doctor_adapter adapter;
    private List<Doctor_realm> doctor_realmList;
    private ApiInterface apiInterface;
    private RealmResults<Doctor_realm> doctor_realmRealmResults;
    private Doctor_realm doctor_realm;
    private RecyclerView recyclerView;
    EditText et_dr_search;


    String[] medical_centre = {"Medical centers","King Faisal - Prime Specialist Medical Center"," Al Nahda - Prime Medical Center","Deira - Prime Medical Center","Sheikh Zayed Road - Prime Medical Center","Al Qasimia - Prime Medical Center", "Bur Dubai - Prime Medical Ceter"};
    String[] department = {"NUTRITION & DIETETICS","ANESTHESIA","GYNECOLOGY","GASTROENTEROLOGY","PATHOLOGY","ICU", "DENTAL"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_doctor);
        getSupportActionBar().setTitle("Find Doctor");



        recyclerView=(RecyclerView)findViewById(R.id.rvfind_dr);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
//        et_dr_search=(EditText)findViewById(R.id.et_dr_search);
//
//
//        Spinner spin=(Spinner)findViewById(R.id.spmedical_centre);
//        Spinner spindepartment =(Spinner)findViewById(R.id.sp_department);
//        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                String medical_selectitem=parent.getItemAtPosition(position).toString();
//                Toast.makeText(FindDoctor.this, medical_selectitem, Toast.LENGTH_SHORT).show();
//                doctor_realmRealmResults = RealmUtils.getInstance().getfinddrwithmedicalcenter(medical_selectitem);
//                Toast.makeText(FindDoctor.this, doctor_realmRealmResults.size(), Toast.LENGTH_SHORT).show();
//                adapter=new doctor_adapter(doctor_realmRealmResults, new MedicalClickListener() {
//                    @Override
//                    public void onClick(String id) {
//                        doctor_realm=RealmUtils.getInstance().getDr_ItemwithId(id);
//
////                Toast.makeText(FindDoctor.this, doctor_realm.getDepartment_name(), Toast.LENGTH_SHORT).show();
//                        Intent intent= new Intent(FindDoctor.this,Doctor_Profile.class);
//                        intent.putExtra("degree",doctor_realm.getDegree());
//                        intent.putExtra("department_name",doctor_realm.getDepartment_name());
//                        intent.putExtra("description",doctor_realm.getDescription());
//                        intent.putExtra("name",doctor_realm.getDoctor_name());
//                        intent.putExtra("hospital_name",doctor_realm.getMedicalcenter_name());
//                        intent.putExtra("telephone",doctor_realm.getTelephone_no());
//                        intent.putExtra("email",doctor_realm.getEmail());
//                        startActivity(intent);
//
//                    }
//                });
//
//                recyclerView.setAdapter(adapter);
//
//
//
//
//
//
//
//
//
//
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        //spin.setOnItemClickListener(this);

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,medical_centre);
        ArrayAdapter medicaladapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,department);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medicaladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
//        spin.setAdapter(aa);
//        spindepartment.setAdapter(medicaladapter);

        apiInterface= ApiClient.getApiClient().create(ApiInterface.class);
        Call<ModelDoctor> call = apiInterface.getDocts();
        call.enqueue(new Callback<ModelDoctor>() {
            @Override
            public void onResponse(Call<ModelDoctor> call, Response<ModelDoctor> response) {
                RealmUtils.getInstance().saveDoctors(response.body().getDoctors());
            }

            @Override
            public void onFailure(Call<ModelDoctor> call, Throwable t) {

            }
        });
        doctor_realmRealmResults = RealmUtils.getInstance().getAllDoctors();
        adapter=new doctor_adapter(doctor_realmRealmResults, new MedicalClickListener() {
            @Override
            public void onClick(String id) {
                doctor_realm=RealmUtils.getInstance().getDr_ItemwithId(id);

//                Toast.makeText(FindDoctor.this, doctor_realm.getDepartment_name(), Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(FindDoctor.this,Doctor_Profile.class);
                intent.putExtra("degree",doctor_realm.getDegree());
                intent.putExtra("department_name",doctor_realm.getDepartment_name());
                intent.putExtra("description",doctor_realm.getDescription());
                intent.putExtra("name",doctor_realm.getDoctor_name());
                intent.putExtra("hospital_name",doctor_realm.getMedicalcenter_name());
                intent.putExtra("telephone",doctor_realm.getTelephone_no());
                intent.putExtra("email",doctor_realm.getEmail());
                startActivity(intent);

            }
        });

recyclerView.setAdapter(adapter);


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
