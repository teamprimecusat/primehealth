package com.primehealth.cusat.primehealth.FindMedical;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.primehealth.cusat.primehealth.R;

import java.util.List;

public class medical_adapter extends RecyclerView.Adapter<medical_adapter.MyViewHolder_Medical_adptr> {
    private List<MedicalCentre> medicalCentresLst;
    private MedicalClickListener medicalClickListener;

    public medical_adapter(List<MedicalCentre> medicalCentresLst,MedicalClickListener medicalClickListener) {
        this.medicalCentresLst = medicalCentresLst;
        this.medicalClickListener=medicalClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder_Medical_adptr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.medical_center_layout_list_item,parent,false);
        return new MyViewHolder_Medical_adptr(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder_Medical_adptr holder, final int position) {
        holder.name.setText(medicalCentresLst.get(position).getMedicalcenter_name());
        holder.address.setText(medicalCentresLst.get(position).getLocation());
        holder.llmedical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicalClickListener.onClick(medicalCentresLst.get(position).getMedicalcenter_id());
            }
        });

    }

    @Override
    public int getItemCount() {
        return medicalCentresLst.size();
    }


    public class MyViewHolder_Medical_adptr extends RecyclerView.ViewHolder
    {
        TextView name,address;
        private LinearLayout llmedical;

        public MyViewHolder_Medical_adptr(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.medical_hospital_name);
            address=(TextView)itemView.findViewById(R.id.medical_hospital_address);
            llmedical=(LinearLayout)itemView.findViewById(R.id.medical_center_parent_layout);
        }
    }

}
