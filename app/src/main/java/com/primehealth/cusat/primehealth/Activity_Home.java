package com.primehealth.cusat.primehealth;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.primehealth.cusat.primehealth.FindDoctor.Doctor_Profile;
import com.primehealth.cusat.primehealth.FindDoctor.FindDoctor;
import com.primehealth.cusat.primehealth.FindMedical.FindMedicalCenter;
import com.primehealth.cusat.primehealth.HealthTips.HealthTips;

import java.util.HashMap;

import static com.daimajia.slider.library.SliderLayout.PresetIndicators.*;
import static com.daimajia.slider.library.SliderLayout.Transformer.*;

public class Activity_Home extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout sliderLayout;
    private HashMap<String, Integer> sliderImages;
    CardView cv_finddoctor,cv_medicalcenter,cv_healthtip,cv_appointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity__home);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().hide();

        cv_finddoctor=findViewById(R.id.cv_finddoctor);
        cv_medicalcenter=findViewById(R.id.cv_medicalcenter);
        cv_healthtip=findViewById(R.id.cv_healthtip);
        cv_appointment=findViewById(R.id.cv_appointment);

        cv_finddoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Activity_Home.this,FindDoctor.class);
                startActivity(intent);
            }
        });
        cv_medicalcenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Activity_Home.this,FindMedicalCenter.class);
                startActivity(intent);
            }
        });
        cv_healthtip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Activity_Home.this,HealthTips.class);
                startActivity(intent);
            }
        });
        cv_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:00971567265371"));
                startActivity(intent);
            }
        });

        sliderLayout = findViewById(R.id.sliderLayout);
        sliderImages = new HashMap<>();
        setupSlider();
    }





    private void setupSlider() {
            sliderLayout = (SliderLayout) findViewById(R.id.sliderLayout);
            sliderImages = new HashMap<>();
            sliderImages.put("Multi-speciality hospital close to you",R.drawable.hospital_6);
            sliderImages.put("Excellence is our speciality",R.drawable.hospital5);
            sliderImages.put("Close to home... Close to your heart", R.drawable.hospital_3);
            sliderImages.put("Care you believe in",R.drawable.hospital_2);
            sliderImages.put("Your health our care", R.drawable.hospital1);

            for (String name : sliderImages.keySet()) {

                TextSliderView textSliderView = new TextSliderView(this);
                textSliderView
                        .description(name)
                        .image(sliderImages.get(name))
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);
                textSliderView.bundle(new Bundle());
                textSliderView.getBundle()
                        .putString("extra", name);
                sliderLayout.addSlider(textSliderView);
            }
            sliderLayout.setPresetTransformer(Accordion);
            sliderLayout.setPresetIndicator(Center_Bottom);
            sliderLayout.setCustomAnimation(new DescriptionAnimation());
            sliderLayout.setDuration(3000);
            sliderLayout.addOnPageChangeListener(this);

        }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }
}
