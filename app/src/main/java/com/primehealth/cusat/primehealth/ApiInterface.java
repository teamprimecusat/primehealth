package com.primehealth.cusat.primehealth;

import com.primehealth.cusat.primehealth.FindDoctor.ModelDoctor;
import com.primehealth.cusat.primehealth.FindMedical.Modelmedical;
import com.primehealth.cusat.primehealth.HealthTips.ModelHealthTip;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by abhij on 28/05/2018.
 */

public interface ApiInterface {

//    @POST("find_doctory_retrieve.php")
//    Call<List<FindDoctorModel>> getDoctorDetails();

    @GET("medical_center_retrieve.php")
    Call<Modelmedical> getMedicals();

    @GET("health_tips_retrieve.php")
    Call<ModelHealthTip> getHealthTip();

    @GET("doctor_profile_retrieve.php")
    Call<ModelDoctor> getDocts();

}
