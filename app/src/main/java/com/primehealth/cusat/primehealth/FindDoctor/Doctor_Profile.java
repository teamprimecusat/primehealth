package com.primehealth.cusat.primehealth.FindDoctor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.primehealth.cusat.primehealth.R;

public class Doctor_Profile extends AppCompatActivity {

    Bundle bundle;
    TextView tv_dr_p_name,tv_dr_p_degree,tv_dr_p_department,
            tv_dr_p_hospital_name,tv_dr_p_phone,
            tv_dr_p_mail,tv_dr_p_description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle("Doctor Profile");

        setContentView(R.layout.activity_doctor_profile);
        Intent intent=getIntent();
        bundle=intent.getExtras();
        String degree=bundle.getString("degree");
        String department_name=bundle.getString("department_name");
        String description=bundle.getString("description");
        String name=bundle.getString("name");
        String hospital_name=bundle.getString("hospital_name");
        String telephone=bundle.getString("telephone");
        String email=bundle.getString("email");
        Toast.makeText(this, email, Toast.LENGTH_SHORT).show();

        tv_dr_p_degree=(TextView)findViewById(R.id.tv_dr_p_degree);
        tv_dr_p_name=(TextView)findViewById(R.id.tv_dr_p_name);
        tv_dr_p_department=(TextView)findViewById(R.id.tv_dr_p_department);
        tv_dr_p_hospital_name=(TextView)findViewById(R.id.tv_dr_p_hospital_name);
        tv_dr_p_phone=(TextView)findViewById(R.id.tv_dr_p_phone);
        tv_dr_p_mail=(TextView)findViewById(R.id.tv_dr_p_mail);
        tv_dr_p_description=(TextView)findViewById(R.id.tv_dr_p_description);

        tv_dr_p_degree.setText(degree);
        tv_dr_p_name.setText(name);
        tv_dr_p_department.setText(department_name);
        tv_dr_p_description.setText(description);
        tv_dr_p_hospital_name.setText(hospital_name);
        tv_dr_p_phone.setText(telephone);




    }
}
