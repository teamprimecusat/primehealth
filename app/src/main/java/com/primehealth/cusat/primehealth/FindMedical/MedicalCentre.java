package com.primehealth.cusat.primehealth.FindMedical;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MedicalCentre extends RealmObject {

    @PrimaryKey
    private String medicalcenter_id;
    private String medicalcenter_name;
    private String Location;
    private String telephone_no;
    private String fax_no;
    private String email;
    private String website;
    private String weekdays;
    private String friday;
    private String saturday;
    private String latitude;
    private String longitude;

    public String getMedicalcenter_id() {
        return medicalcenter_id;
    }

    public String getMedicalcenter_name() {
        return medicalcenter_name;
    }

    public String getLocation() {
        return Location;
    }

    public String getTelephone_no() {
        return telephone_no;
    }

    public String getFax_no() {
        return fax_no;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public String getWeekdays() {
        return weekdays;
    }

    public String getFriday() {
        return friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

}

