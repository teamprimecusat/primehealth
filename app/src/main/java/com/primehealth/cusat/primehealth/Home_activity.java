package com.primehealth.cusat.primehealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.primehealth.cusat.primehealth.FindDoctor.Doctor_Profile;
import com.primehealth.cusat.primehealth.FindDoctor.FindDoctor;
import com.primehealth.cusat.primehealth.FindMedical.FindMedicalCenter;
import com.primehealth.cusat.primehealth.HealthTips.HealthTips;

public class Home_activity extends AppCompatActivity {
    CardView cv_finddoctor,cv_medicalcenter,cv_healthtip,cv_appointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_activity);

        cv_finddoctor=findViewById(R.id.cv_finddoctor);
        cv_medicalcenter=findViewById(R.id.cv_medicalcenter);
        cv_healthtip=findViewById(R.id.cv_healthtip);
        cv_appointment=findViewById(R.id.cv_appointment);

        cv_finddoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Home_activity.this,FindDoctor.class);
                startActivity(intent);
            }
        });
        cv_medicalcenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Home_activity.this,FindMedicalCenter.class);
                startActivity(intent);
            }
        });
        cv_healthtip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Home_activity.this,HealthTips.class);
                startActivity(intent);
            }
        });
        cv_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Home_activity.this,Doctor_Profile.class);
                startActivity(intent);
            }
        });
    }
}
