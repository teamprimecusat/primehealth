package com.primehealth.cusat.primehealth.HealthTips;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class HealthTipsRealmobj extends RealmObject {

    @PrimaryKey
    private String health_tips_id;
    private String health_tips_heading;
    private String health_tips_small_desc;
    private String health_tips_desc;

    public String getHealth_tips_id() {
        return health_tips_id;
    }

    public String getHealth_tips_heading() {
        return health_tips_heading;
    }

    public String getHealth_tips_small_desc() {
        return health_tips_small_desc;
    }

    public String getHealth_tips_desc() {
        return health_tips_desc;
    }




}
