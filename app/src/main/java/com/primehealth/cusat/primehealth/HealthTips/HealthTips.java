package com.primehealth.cusat.primehealth.HealthTips;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.primehealth.cusat.primehealth.ApiClient;
import com.primehealth.cusat.primehealth.ApiInterface;
import com.primehealth.cusat.primehealth.FindMedical.MedicalClickListener;
import com.primehealth.cusat.primehealth.R;
import com.primehealth.cusat.primehealth.RealmUtils;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthTips extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;

    private RecyclerView.LayoutManager layoutManager;
    private health_tips_adapter adapter;
    private List<HealthTipsRealmobj> healthTipsRealmobjs;
    private ApiInterface apiInterface;
    private RealmResults<HealthTipsRealmobj> healthTipsRealmobjRealmResults;
    private HealthTipsRealmobj healthTipsRealmobj;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_tips);
        getSupportActionBar().setTitle("Health Tips");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager=(ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {viewPager = (ViewPager) findViewById(R.id.viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HealthtipsFragment1(), "All");
        adapter.addFragment(new HealthtipsFragment2(), "DOCTOR INTERVIEW");

        viewPager.setAdapter(adapter);
    }
}

class ViewPagerAdapter extends FragmentPagerAdapter
{
    private final List<Fragment> allList = new ArrayList<>();
    private final List<String> interviewList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager)
    {

        super(manager);
    }
    @Override
    public Fragment getItem(int position) {

        return allList.get(position);
    }
    @Override
    public int getCount()
    {
        return allList.size();
    }
    public void addFragment(Fragment alllist, String interview) {
        allList.add(alllist);
        interviewList.add(interview);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return interviewList.get(position);
    }
}